package com.michael.HelpDesk.api.enums;

public enum PriorityEnum {

	High,
	Normal,
	Low;
}
