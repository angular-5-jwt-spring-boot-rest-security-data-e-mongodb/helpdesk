package com.michael.HelpDesk.api.enums;

public enum StatusEnum {

	New,
	Assigned,
	Resolved,
	Approved,
	Disoproved,
	Closed;
	
	public static StatusEnum getStatusEnum(String statusRequirido) {
		for (StatusEnum d : StatusEnum.values()) {
			if(d.equals(statusRequirido)) {
				return d;
			}
		}
		
		throw new IllegalArgumentException("Status Inexistente!");
	}
}
