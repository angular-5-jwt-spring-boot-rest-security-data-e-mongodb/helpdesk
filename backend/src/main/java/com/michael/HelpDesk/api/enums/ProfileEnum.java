package com.michael.HelpDesk.api.enums;

public enum ProfileEnum {
	
	ROLE_ADMIN,
	ROLE_CUSTOMER,
	ROLE_TECHINICIAN;
}
