package com.michael.HelpDesk.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.michael.HelpDesk.api.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

	public User findByEmail(String email);
}
